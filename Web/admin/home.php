<?php

include('session.php');
if(!isset($_SESSION['username'])){
	header("location: login-form.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<title> Home Page </title>
</head>
	<body>
		<!DOCTYPE html>
		<html>
		<head>
			<title> Home Page </title>
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
			<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		</head>

			<body>
				<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: steelblue  ; border-bottom-width: 2px;  border-bottom-color: steelblue; ">
			        <div class="container-fluid">
			            <div class="navbar-header">
			                <h4 style="color:black;"><small style="color:white; margin-left: 10px;">Version 1.2.0</small></h4>
			            </div>
			        </div>
			    </div>
					<div class="container-fluid" style="font-size:12px;">
		                <div class="row">
		                    <div class="col-sm-3 col-md-2 sidebar" style="position:fixed; width: 150px; height:100%; background-color: steelblue; border-right-color:steelblue; min-width:100px; ">
		                        <ul class="nav nav-sidebar" style="font-weight: bold; margin-top: 40px;">
		                            <li id="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="home.php">Αρχική</a></li>
		                            <li id ="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="visualize.php">Απεικόνιση στοιχείων</a></li>
		                            <li id="manage"><a style="color: white; font-family : Times New Romans; font-size : 16px " href="delete.php">Διαγραφή Δεδομένων</a></li>
		                            <li id="lougout"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="logout.php">ΑΠΟΣΥΝΔΕΣΗ</a></li>
		                        </ul>
		                    </div>

		                </div>
		            </div>
								<div style="padding-left: 200px;">
									<table id='total' class="table">
									<thead style=" padding-left: 200px; width:20px;">
									<tr style="font-size:small;">

									<th style="font-size:small;" scope="col">IN_VEHICLE</th>
									<th style="font-size:small;" scope="col">ON_BICYCLE</th>
									<th style="font-size:small;" scope="col">ON_FOOT</th>
									<th style="font-size:small;" scope="col">RUNNING</th>
									<th style="font-size:small;" scope="col">STILL</th>
									<th style="font-size:small;" scope="col">TILTING</th>
									<th style="font-size:small;" scope="col">UNKNOWN</th>
									<th style="font-size:small;" scope="col">WALKING</th>
									<th style="font-size:small;" scope="col">EXITING_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_ROAD_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_FOUR_WHEELER_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_TWO_WHEELER_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_BUS</th>
									<th style="font-size:small;" scope="col">IN_CAR</th>
									</tr>
									</thead >
									<tbody >
									<tr>
									</tr>
								</tbody>
								</table>
								<hr class = 'mb-3'>
								<table id='users' class="table">
								<thead style=" padding-left: 200px; width:20px;">
								<tr>

								</tr>
								</thead >
								<tbody >
								<tr>
								</tr>
							</tbody>
							</table>
							<hr class = 'mb-3'>
							<table id='hour' class="table">
							<thead style=" padding-left: 200px; width:20px;">
							<tr style="font-size:small;">

							<th style="font-size:small;" scope="col">0</th>
							<th style="font-size:small;" scope="col">1</th>
							<th style="font-size:small;" scope="col">2</th>
							<th style="font-size:small;" scope="col">3</th>
							<th style="font-size:small;" scope="col">4</th>
							<th style="font-size:small;" scope="col">5</th>
							<th style="font-size:small;" scope="col">6</th>
							<th style="font-size:small;" scope="col">7</th>
							<th style="font-size:small;" scope="col">8</th>
							<th style="font-size:small;" scope="col">9</th>
							<th style="font-size:small;" scope="col">10</th>
							<th style="font-size:small;" scope="col">11</th>
							<th style="font-size:small;" scope="col">12</th>
							<th style="font-size:small;" scope="col">13</th>
							<th style="font-size:small;" scope="col">14</th>
							<th style="font-size:small;" scope="col">15</th>
							<th style="font-size:small;" scope="col">16</th>
							<th style="font-size:small;" scope="col">17</th>
							<th style="font-size:small;" scope="col">18</th>
							<th style="font-size:small;" scope="col">19</th>
							<th style="font-size:small;" scope="col">20</th>
							<th style="font-size:small;" scope="col">21</th>
							<th style="font-size:small;" scope="col">22</th>
							<th style="font-size:small;" scope="col">23</th>
							</tr>
							</thead >
							<tbody >
							<tr>
							</tr>
						</tbody>
						</table>
						<hr class = 'mb-3'>
						<table id='day' class="table">
						<thead style=" padding-left: 200px; width:20px;">
						<tr style="font-size:small;">

						<th style="font-size:small;" scope="col">1</th>
						<th style="font-size:small;" scope="col">2</th>
						<th style="font-size:small;" scope="col">3</th>
						<th style="font-size:small;" scope="col">4</th>
						<th style="font-size:small;" scope="col">5</th>
						<th style="font-size:small;" scope="col">6</th>
						<th style="font-size:small;" scope="col">7</th>
						<th style="font-size:small;" scope="col">8</th>
						<th style="font-size:small;" scope="col">9</th>
						<th style="font-size:small;" scope="col">10</th>
						<th style="font-size:small;" scope="col">11</th>
						<th style="font-size:small;" scope="col">12</th>
						<th style="font-size:small;" scope="col">13</th>
						<th style="font-size:small;" scope="col">14</th>
						<th style="font-size:small;" scope="col">15</th>
						<th style="font-size:small;" scope="col">16</th>
						<th style="font-size:small;" scope="col">17</th>
						<th style="font-size:small;" scope="col">18</th>
						<th style="font-size:small;" scope="col">19</th>
						<th style="font-size:small;" scope="col">20</th>
						<th style="font-size:small;" scope="col">21</th>
						<th style="font-size:small;" scope="col">22</th>
						<th style="font-size:small;" scope="col">23</th>
						<th style="font-size:small;" scope="col">24</th>
						<th style="font-size:small;" scope="col">25</th>
						<th style="font-size:small;" scope="col">26</th>
						<th style="font-size:small;" scope="col">27</th>
						<th style="font-size:small;" scope="col">28</th>
						<th style="font-size:small;" scope="col">29</th>
						<th style="font-size:small;" scope="col">30</th>
						<th style="font-size:small;" scope="col">31</th>
						</tr>
						</thead >
						<tbody >
						<tr>
						</tr>
					</tbody>
					</table>
					<table id='month' class="table">
					<thead style=" padding-left: 200px; width:20px;">
					<tr style="font-size:small;">

					</tr>
					</thead >
					<tbody >
					<tr>
					</tr>
				</tbody>
				</table>
				<table id='years' class="table">
				<thead style=" padding-left: 200px; width:20px;">
				<tr>

				</tr>
				</thead >
				<tbody >
				<tr>
				</tr>
			</tbody>
			</table>
			<hr class = 'mb-3'>
			<hr class = 'mb-3'>
			<button class="btn btn-danger">Διαγραφή Δεδομένων</button>
							</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
window.onload = function(){

	$.ajax({
		type:'POST',
		url : 'actions/totalactivity.php',
		success:function(data){
			var total = JSON.parse(data);
			 var datachart = [];
			 $("#total").find('td').each(function(){$(this).remove();});
			 for(i = 0; i<total.length; i++){
				var markup = "<td>" + total[i] + "</td>";
				$("#total").find('tbody').append(markup);
			}
			//$("#total").find('tbody').append("<tr></tr>");
		},
		error:function(data){}
	});
	$.ajax({
		type:'POST',
		url :'actions/alluseractivity.php',
		success:function(data){
			var total = JSON.parse(data);
			 var datachart = [];
			 //$("#users").find('td').each(function(){$(this).remove();});
			 for(i = 0; i<total.length; i++){
				 if( i%2 == 0 ){
				 var th = "<th style='font-size:small;'' scope='col'>" + total[i] + "</th>";
				 $("#users").find('thead').append(th);
			 }
				 if( i%2 != 0){
				var markup = "<td>" + total[i] + "</td>";
				$("#users").find('tbody').append(markup);
			}
			}
			//$("#total").find('tbody').append("<tr></tr>");
		},
		error:function(data){}
	});
	$.ajax({
		type:'POST',
		url :'actions/activity.php',
		success:function(data){
			var dataphp = JSON.parse(data);
			alert(dataphp);
			 var datachart = [];
			 //$("#hour").find('td').each(function(){$(this).remove();});
			 for(i = 0; i<=23; i++){
				var markup = "<td>" + dataphp[i] + "</td>";
				$("#hour").find('tbody').append(markup);
			}
			for(i = 24; i<=54; i++){
			 var markup = "<td>" + dataphp[i] + "</td>";
			 $("#day").find('tbody').append(markup);
		 }
		//  for(i = 55; i<=65; i++){
		// 	 alert(dataphp[i]);
		// 	var markup = "<td>" + dataphp[i] + "</td>";
		// 	$("#month").find('tbody').append(markup);
		// }

		for(i = 55; i< dataphp.length; i++){
			if(i%2 != 0){
				var th = "<th style='font-size:small;'' scope='col'>" + dataphp[i] + "</th>";
				$("#years").find('thead').append(th);
			}
			else{
		 var markup = "<td>" + dataphp[i] + "</td>";
		 $("#years").find('tbody').append(markup);
	 }
	 }
			//$("#total").find('tbody').append("<tr></tr>");
		},
		error:function(data){}
	});
	$.ajax({
		type:'POST',
		url :'actions/monthactivity.php',
		success:function(data){
			var datamonth = JSON.parse(data);

			 var datachart = [];

		 for(i = 0; i< datamonth.length; i++){
			 if(i%2 == 0){
				var th = "<th style='font-size:small;'' scope='col'>" + datamonth[i] + "</th>";
	 			$("#month").find('thead').append(th);
			}
			else{
				var markup = "<td>" + datamonth[i] + "</td>";
				$("#month").find('tbody').append(markup);
			}
		}


		},
		error:function(data){}
	});
}
</script>
<script>
	$(".btn-danger").on('click',function() {
		Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
		$.ajax({
			type:'GET',
			url :'actions/deleteall.php',
			error:function(data){
				alert('ΣΦΑΛΜΑ');
			}
		});
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
  }
})

})
</script>
	</body>
</html>
