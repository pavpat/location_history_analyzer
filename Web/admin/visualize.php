<?php

include('session.php');
if(!isset($_SESSION['username'])){
	header("location: login-form.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<title> Home Page </title>
</head>
	<body>
		<!DOCTYPE html>
		<html>
		<head>
			<title> Home Page </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
			<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>	-->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		</head>

			<body>
				<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: steelblue  ; border-bottom-width: 2px;  border-bottom-color: steelblue; ">
			        <div class="container-fluid">
			            <div class="navbar-header">
			                <h4 style="color:black;"><small style="color:white; margin-left: 10px;">Version 1.2.0</small></h4>
			            </div>
			        </div>
			    </div>
					<div class="container-fluid" style="font-size:12px;">
		                <div class="row">
		                    <div class="col-sm-3 col-md-2 sidebar" style="position:fixed; width: 150px; height:100%; background-color: steelblue; border-right-color:steelblue; min-width:100px; ">
		                        <ul class="nav nav-sidebar" style="font-weight: bold; margin-top: 40px;">
		                            <li id="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="home.php">Αρχική</a></li>
		                            <li id ="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="visualize.php">Απεικόνιση στοιχείων</a></li>
		                            <li id="manage"><a style="color: white; font-family : Times New Romans; font-size : 16px " href="delete.php">Διαγραφή Δεδομένων</a></li>
		                            <li id="lougout"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="logout.php">ΑΠΟΣΥΝΔΕΣΗ</a></li>
		                        </ul>
		                    </div>

		                </div>

										<div class="dates">
											<div class="start_date input-group mb-3" >
												<input class="form-control start_date" type="text" placeholder="start date" id="startdate_datepicker">
											</div>
											<div class="end_date input-group mb-3">
												<input  class="form-control end_date" type="text" placeholder="end date" id="enddate_datepicker">
											</div>
											<button type="button" id='btn' style="margin-left: 200px;" class="btn btn-primary">Get Results</button>
										</div>

										<div class="dropdown">
								    <a class="btn btn-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> ΔΡΑΣΤΗΡΙΟΤΗΤΑ</a>
								    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								    <a class="dropdown-item">IN_VEHICLE</a>
								    <a class="dropdown-item">ON_BICYCLE</a>
								    <a class="dropdown-item">ON_FOOT</a>
										<a class="dropdown-item">RUNNING</a>
										<a class="dropdown-item">STILL</a>
										<a class="dropdown-item">TILTING</a>
										<a class="dropdown-item">UNKNOWN</a>
										<a class="dropdown-item">WALKING</a>
										<a class="dropdown-item">EXITING_VEHICLE</a>
										<a class="dropdown-item">IN_ROAD_VEHICLE</a>
										<a class="dropdown-item">IN_FOUR_WHEELER_VEHICLE</a>
										<a class="dropdown-item">IN_TWO_WHEELER_VEHICLE</a>
										<a class="dropdown-item">IN_BUS</a>
										<a class="dropdown-item">IN_CAR</a>
										<a class="dropdown-item">ΟΛΑ</a>
								    </div>
								    </div>

		            </div>
								<div style="padding-left: 200px;">
									<table id='total' class="table">
									<thead style=" padding-left: 200px; width:20px;">
									<tr style="font-size:small;">

									<th style="font-size:small;" scope="col">IN_VEHICLE</th>
									<th style="font-size:small;" scope="col">ON_BICYCLE</th>
									<th style="font-size:small;" scope="col">ON_FOOT</th>
									<th style="font-size:small;" scope="col">RUNNING</th>
									<th style="font-size:small;" scope="col">STILL</th>
									<th style="font-size:small;" scope="col">TILTING</th>
									<th style="font-size:small;" scope="col">UNKNOWN</th>
									<th style="font-size:small;" scope="col">WALKING</th>
									<th style="font-size:small;" scope="col">EXITING_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_ROAD_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_FOUR_WHEELER_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_TWO_WHEELER_VEHICLE</th>
									<th style="font-size:small;" scope="col">IN_BUS</th>
									<th style="font-size:small;" scope="col">IN_CAR</th>
									</tr>
									</thead >
									<tbody >
									<tr>
									</tr>
								</tbody>
								</table>
								<table id='users' class="table">
								<thead style=" padding-left: 200px; width:20px;">
								<tr>

								</tr>
								</thead >
								<tbody >
								<tr>
								</tr>
							</tbody>
							</table>
							<table id='hour' class="table">
							<thead style=" padding-left: 200px; width:20px;">
							<tr style="font-size:small;">

							<th style="font-size:small;" scope="col">0</th>
							<th style="font-size:small;" scope="col">1</th>
							<th style="font-size:small;" scope="col">2</th>
							<th style="font-size:small;" scope="col">3</th>
							<th style="font-size:small;" scope="col">4</th>
							<th style="font-size:small;" scope="col">5</th>
							<th style="font-size:small;" scope="col">6</th>
							<th style="font-size:small;" scope="col">7</th>
							<th style="font-size:small;" scope="col">8</th>
							<th style="font-size:small;" scope="col">9</th>
							<th style="font-size:small;" scope="col">10</th>
							<th style="font-size:small;" scope="col">11</th>
							<th style="font-size:small;" scope="col">12</th>
							<th style="font-size:small;" scope="col">13</th>
							<th style="font-size:small;" scope="col">14</th>
							<th style="font-size:small;" scope="col">15</th>
							<th style="font-size:small;" scope="col">16</th>
							<th style="font-size:small;" scope="col">17</th>
							<th style="font-size:small;" scope="col">18</th>
							<th style="font-size:small;" scope="col">19</th>
							<th style="font-size:small;" scope="col">20</th>
							<th style="font-size:small;" scope="col">21</th>
							<th style="font-size:small;" scope="col">22</th>
							<th style="font-size:small;" scope="col">23</th>
							</tr>
							</thead >
							<tbody >
							<tr>
							</tr>
						</tbody>
						</table>
							</div>
							<link rel="stylesheet" href="javascript/datepicker/css/datepicker.css" />
		 <script src="javascript/datepicker/js/bootstrap-datepicker.js"></script>
<script>
window.onload = function(){

	$(".dropdown-menu a").click(function(){

	      $(".btn:first-child").text($(this).text());

	      $(".btn:first-child").val($(this).text());
			});

			$('#startdate_datepicker').datepicker({

					multipledate: true
					//dateformat: 'yyyy-mm-dd hh'
			});
			$('#enddate_datepicker').datepicker({

					multipledate: true
					//format: 'yyyy-mm-dd :hh'

			});
			$('#btn').on('click',function(e){
			var	strDate = $('#startdate_datepicker').val();

			var	endDate = $('#enddate_datepicker').val();
			var type = $('#dropdownMenuLink').val();
			alert(type);
	$.ajax({
		type:'POST',
		url : 'actions/activitydata.php',
		data :{strDate: strDate, endDate : endDate , type:type},
		success:function(data){
			var total = JSON.parse(data);
			 var datachart = [];
			 $("#total").find('td').each(function(){$(this).remove();});
			 for(i = 0; i<=13; i++){
				var markup = "<td>" + total[i] + "</td>";
				$("#total").find('tbody').append(markup);
			}
			//$("#total").find('tbody').append("<tr></tr>");
		},
		error:function(data){}
	});
})
	// $.ajax({
	// 	type:'POST',
	// 	url :'actions/alluseractivity.php',
	// 	success:function(data){
	// 		var total = JSON.parse(data);
	// 		 var datachart = [];
	// 		 //$("#users").find('td').each(function(){$(this).remove();});
	// 		 for(i = 0; i<total.length; i++){
	// 			 if( i%2 == 0 ){
	// 			 var th = "<th style='font-size:small;'' scope='col'>" + total[i] + "</th>";
	// 			 $("#users").find('thead').append(th);
	// 		 }
	// 			 if( i%2 != 0){
	// 			var markup = "<td>" + total[i] + "</td>";
	// 			$("#users").find('tbody').append(markup);
	// 		}
	// 		}
	// 		//$("#total").find('tbody').append("<tr></tr>");
	// 	},
	// 	error:function(data){}
	// });
	// $.ajax({
	// 	type:'POST',
	// 	url :'actions/activity.php',
	// 	success:function(data){
	// 		var hour = JSON.parse(data);
	// 		 var datachart = [];
	// 		 //$("#hour").find('td').each(function(){$(this).remove();});
	// 		 for(i = 0; i<hour.length; i++){
	// 			var markup = "<td>" + hour[i] + "</td>";
	// 			$("#hour").find('tbody').append(markup);
	//
	// 		}
	// 		//$("#total").find('tbody').append("<tr></tr>");
	// 	},
	// 	error:function(data){}
	// });
}

</script>
<style>
#manage :hover {
		background-color:cadetblue;
}

#hover :hover {
		background-color:cadetblue;
}

#pdf :hover {
		background-color : cadetblue;
}
#startdate_datepicker{
	margin-top: 20px;
	margin-left: 200px;
}
#enddate_datepicker{
	margin-top: 20px;
	margin-left: 200px;
}
.dropdown{
		margin-top: 20px;
		padding-left: 200px;
		margin-bottom: 20px;
}
</style>
	</body>
</html>
