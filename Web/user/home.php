<?php
include('session.php');
if(!isset($_SESSION['username'])){
	header("location: login.html");
	echo $password;
}

include('actions/script.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title> Home Page </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
</head>

	<body>
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: steelblue  ; border-bottom-width: 2px;  border-bottom-color: steelblue; ">
	        <div class="container-fluid">
	            <div class="navbar-header">
	                <h4 style="color:black;"><small style="color:white; margin-left: 10px;">Version 1.2.0</small></h4>
	            </div>

	        </div>
	    </div>
			<div class="container-fluid" style="font-size:12px;">
                <div class="row">
                    <div class="col-sm-3 col-md-2 sidebar" style="position:fixed; width: 150px; height:100%; background-color: steelblue; border-right-color:steelblue; min-width:100px; ">
                        <ul class="nav nav-sidebar" style="font-weight: bold; margin-top: 40px;">
                            <li id="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="home.php">Αρχική</a></li>
                            <li id ="hover"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="data.php">Απεικόνιση στοιχείων</a></li>
                            <li id="manage"><a style="color: white; font-family : Times New Romans; font-size : 16px " href="insertdata.php">Εισαγωγή Δεδομένων</a></li>
                            <li id="manage"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="">Leaderboards</a></li>
                            <li id="lougout"><a style="color: white; font-family : Times New Romans; font-size : 16px" href="logout.php">ΑΠΟΣΥΝΔΕΣΗ</a></li>
                        </ul>
                    </div>

                </div>
            </div>
						<div style="padding-top: 100px;"class="container">
							<div class = "row">
							<div class="card">
							<div class="card-header">Score οικολογικής μετακίνησης</div>
  						<div class="card-body">
								<h4 class = "small font-weight-bold"> Μηναιο score <span class="float-right"> <?php echo $score?> %</span> </h4>
							<div style = "max-width:700px;" class="progress">
	  						<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $score?>% "></div>
						</div>
							<h4 class = "small font-weight-bold"> Ετησιο score <span class="float-right"> <?php echo $score12?> %</span> </h4>
						<div style = "max-width:700px;" class="progress">
    					<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $score12?>%"> </div>
  					</div>
						<hr class = 'mb-3'>
						<div> H περίοδος που καλύπτουν οι εγγραφές : <?php print_r($minimumdate)  ?> έως <?php print_r($maximumdate) ?></div>
						<hr class = 'mb-3'>
						<div> H ημερομηνία τελευταίου upload : <?php echo $latestupload; ?></div>
					</div>
				</div>
				<div>
					<table id='board' class="table">
					<thead style=" padding-left: 200px; width:20px;">
					<tr>
						<th style="font-size:small;"> Καταταξη</th>
						<th style="font-size:small;"> Username </th>
						<th style="font-size:small;"> Score</th>
					</tr>
					</thead >
					<tbody >

				</tbody>
				</table>
				</div>
</div>
<div class = "row">

						<!--<div style="padding-top:120px ;padding-left: 300px;"> SCORE for last 12 months : //if($totalscore12 != 0){echo ($score12 / $totalscore12)*100 . "%";}else {echo "δεν υπαρχουν εγγραφες για τους τελευταιους 12 μηνες";} ?>  </div>	-->
						<!-- <div id="chartContainer" style="height: 370px; width: 700px; padding-top:100px ;padding-left: 300px"></div> -->
					</div>
				</div>

		<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>-->
		<script>
		window.onload = function () {
			$.ajax({
				type:'POST',
				url :'actions/leaderboard.php',
				success:function(data){
					var leaderboard = JSON.parse(data);
					alert(leaderboard);
					var index = 1;
					while(index < leaderboard.length){
					var max = leaderboard[index];
					for(i=index; i < leaderboard.length; i = i +2){
						if(leaderboard[i] > max){
							tempscore = leaderboard[index];
							tempusername = leaderboard[index - 1]
							leaderboard[index] = leaderboard[i];
							leaderboard[index - 1] = leaderboard[i-1];
							leaderboard[i] = tempscore ;
							leaderboard[i-1] = tempusername;
						}
					}
					index = index + 2 ;
				}
				alert(leaderboard);
				var x = 1;
			for(i=0; i < 5; i = i+2){

					var markup = "<tr><td>" + x + "</td><td>"+ leaderboard[i] + "</td><td>"+ leaderboard[i+1]+"</td></tr>";
					$("#board").find('tbody').append(markup);
					x++;
			}
			for(i = 0; i <leaderboard.length; i = i +2)
			{
				if(leaderboard[i] == '<?php echo $_SESSION['username']?>'){
					var markup = "<tr><td>" + (i/2 + 1) + "</td><td>"+ leaderboard[i] + "</td><td>"+ leaderboard[i+1]+"</td></tr>";
					$("#board").find('tbody').append(markup);
				}
			}
				},
				error:function(data){}

			});
		}
		</script>
		<style>
#manage :hover {
		background-color:cadetblue;
}

#hover :hover {
		background-color:cadetblue;
}

#pdf :hover {
		background-color : cadetblue;
}


.canvasjs-chart-toolbar{
	padding-left: 50px;
}
</style>
	</body>
</html>
